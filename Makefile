# MAINTAINER: Alen Komljen <akomljen.com>

CHARTS = $(shell ls -d */ | grep -v charts)

package:
	helm package $(CHARTS) -d charts
	helm repo index charts --merge charts/index.yaml
	git add . && git commit -a -m "auto update" && git push

helm:
	kubectl -n kube-system create sa tiller
	kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
	kubectl -n kube-system patch deploy/tiller-deploy -p '{"spec": {"template": {"spec": {"serviceAccountName": "tiller"}}}}'
	helm init --service-account tiller

repo:
	helm repo add itc3-public https://gitlab.com/itc3-public/charts/raw/master/charts
	helm update
	helm repo update

user:
	kubectl create clusterrolebinding jeremypogue --clusterrole=cluster-admin --user=jeremypogue@itc3.io
